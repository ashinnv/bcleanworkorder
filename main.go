package main

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/exec"
	"strconv"
	"time"
)

type req struct {
	Cname string
	Cnumb string
	Cdate string
	Ctext string
}

type postGet struct {
	Success bool
	PDFLoc  string
}

func main() {

	jsFunc := genJsFunc()
	indx := genIndex()

	http.Handle("/pdfs/", http.StripPrefix("/pdfs", http.FileServer(http.Dir("./pdfs"))))
	http.HandleFunc("/jquery.js", jsFunc)
	http.HandleFunc("/recv", genRec())
	http.HandleFunc("/", indx)

	http.ListenAndServe(":8085", nil)

	for {
		time.Sleep(100000 * time.Hour)
	}
}

func genJsFunc() func(writer http.ResponseWriter, request *http.Request) {
	return func(writer http.ResponseWriter, request *http.Request) {
		log.Println("JS Started")
		jsDat, err := os.ReadFile("./jquery.js")
		if err != nil {
			fmt.Println("ERR:", err)
		}

		writer.Header().Set("Content-Type", "text/javascript")
		writer.Write(jsDat)

		log.Println("js READ")
	}

}

func genIndex() func(writer http.ResponseWriter, request *http.Request) {
	return func(writer http.ResponseWriter, request *http.Request) {
		indexData, _ := os.ReadFile("./index.html")
		writer.Write(indexData)
		log.Println("ind READ")
	}
}

func genRec() func(writer http.ResponseWriter, request *http.Request) {
	return func(writer http.ResponseWriter, request *http.Request) {

		fmt.Println("GenReq started")

		decReq := &req{}
		err := json.NewDecoder(request.Body).Decode(&decReq)
		if err != nil {
			fmt.Println("Error in JSON decode: ", err)
		}

		sendoff := &postGet{
			Success: false,
			PDFLoc:  "",
		}

		sendoff.Success, sendoff.PDFLoc = buildPDF(*decReq)

		sendData, _ := json.Marshal(sendoff)
		writer.Write([]byte(sendData))

	}
}

func buildPDF(input req) (bool, string) {
	bfr := ""
	finString := ""

	nam := input.Cname
	num := input.Cnumb
	date := input.Cdate
	text := input.Ctext

	finString = finString + "## Customer Name:\n" + nam + "\n\n## Phone Number:\n" + num + "\n\n## Date: " + date + "\n\n## Work Description:\n" + text

	h := hmac.New(sha256.New, []byte(finString))
	h.Write([]byte(bfr))

	sha := hex.EncodeToString(h.Sum(nil))
	tmpFileName := strconv.Itoa(int(time.Now().UnixNano())) + sha

	os.WriteFile(tmpFileName+".md", []byte(finString), 0777)

	cmd := exec.Command("pandoc", "-s", "-o", "pdfs/"+tmpFileName+".pdf", tmpFileName+".md")
	errr := cmd.Run()
	if errr != nil {
		fmt.Println("ERROR:", errr)
		return true, ""
	} else {
		rmcmd := exec.Command("mv", tmpFileName+".md", "md/"+tmpFileName+".md")
		rmcmd.Run()
	}
	fmt.Println("NO Error!")
	return false, tmpFileName

}
